<?php
/**
 * @package WordPress
 * @subpackage wp_starter
 * @since v1.0
 **/

$the_user =  wp_get_current_user();
if( $the_user->ID == 1 ) {
	require( get_template_directory() . '/inc/plugin-activation/plugins.php' );
}

require( get_template_directory() . '/inc/init.php' );
add_action( 'after_setup_theme', 'wp_starter_setup' );

load_class( array( 
	'class-admin', 
	'class-front',
	'class-navigation'
) );


if ( ! function_exists( 'wp_starter_setup' ) ) {
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 */
	function wp_starter_setup() {

		load_theme_textdomain( 'wp_starter', get_template_directory() . '/languages' );

		add_editor_style();
		add_redneck_login();
		add_redneck_admin();

		if ( !function_exists( 'of_get_option' ) ) {
			function of_get_option($name, $default = false) {
				
				$optionsframework_settings = get_option('optionsframework');
				
				// Gets the unique option id
				$option_name = $optionsframework_settings['id'];
				
				if ( get_option($option_name) ) {
					$options = get_option($option_name);
				}
					
				if ( isset($options[$name]) ) {
					return $options[$name];
				} else {
					return $default;
				}
			}
		}

		//load_helper( array('date-time', 'string', 'statistics', 'images', 'navigation') );
		load_helper( array('media', 'string', 'navigation' ) );
		load( array( 'post-meta-api', 
					 'ajax-calls',
					 'behaviors',
				) );


		// Add default posts and comments RSS feed links to <head>.
		add_theme_support( 'automatic-feed-links' );

		// add_theme_support( 'woocommerce' );
		// add_theme_support( 'wc-product-gallery-zoom' );
		// add_theme_support( 'wc-product-gallery-lightbox' );
		// add_theme_support( 'wc-product-gallery-slider' );

		// remove menu container div
		function my_wp_nav_menu_args( $args = '' )
		{
		    $args['container'] = false;
		    return $args;
		} // function
		add_filter( 'wp_nav_menu_args', 'my_wp_nav_menu_args' );

		// Add support for a variety of post formats
		// add_theme_support( 'post-formats', array( 'aside', 'link', 'gallery', 'status', 'quote', 'image' ) );

		// This theme uses Featured Images (also known as post thumbnails) for per-post/per-page Custom Header images
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'slider', 1920, 830, true );

	}
} // wp_starter_setup


if( !is_admin() ){
	add_action('wp_enqueue_scripts', 'wp_starter_default_scripts');
}
function wp_starter_default_scripts() {

	wp_deregister_script( 'jquery' );
	
	wp_register_script( "jquery", '//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js', array(), '3.4.1', true );
	wp_register_script( "script", get_template_directory_uri().'/js/script.js', array(), '1.0', true );
	// wp_register_script( "social-share", get_template_directory_uri().'/inc/theme-addons/social-share/social-share-kit.js', array(), '1.0', true );

	wp_enqueue_script('jquery');
	wp_enqueue_script('script');
	// wp_enqueue_script('social-share');

}


function wp_starter_excerpt_length( $length ) {
	return 38;
}
add_filter( 'excerpt_length', 'wp_starter_excerpt_length' );

function wp_starter_continue_reading_link() {
	return "";
}

function wp_starter_auto_excerpt_more( $more ) {
	return ' &hellip;' . wp_starter_continue_reading_link();
}
add_filter( 'excerpt_more', 'wp_starter_auto_excerpt_more' );


function wp_starter_custom_excerpt_more( $output ) {
	if ( has_excerpt() && ! is_attachment() ) {
		$output .= wp_starter_continue_reading_link();
	}
	return $output;
}
add_filter( 'get_the_excerpt', 'wp_starter_custom_excerpt_more' );

/**
 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
 */
function wp_starter_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'wp_starter_page_menu_args' );

function content_nav( $nav_id ="pagination" ) {
	global $wp_query;

	if ( $wp_query->max_num_pages > 1 ) : ?>
	        <li class="<?php echo $nav_id; ?>">
                <ul>
                    <li class="prev"><?php next_posts_link( __('next') ); ?></li>
                    <li class="next"><?php previous_posts_link( __('prev') ); ?></li>
                </ul>
            </li>
	<?php endif;
}


function wp_starter_body_classes( $classes ) {

	if ( function_exists( 'is_multi_author' ) && ! is_multi_author() )
		$classes[] = 'single-author';

	if ( is_singular() && ! is_home() && ! is_page_template( 'showcase.php' ) && ! is_page_template( 'sidebar-page.php' ) )
		$classes[] = 'singular';

	return $classes;
}
add_filter( 'body_class', 'wp_starter_body_classes' );


// dodajemo excerpt support i u page
add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
    add_post_type_support( 'page', 'excerpt' );
}


// wpml list langugae
function switch_language(){
	if( defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE != "" ) {
	    $languages = icl_get_languages('skip_missing=0&orderby=id');
		//array_reverse($languages);
		//print_r( $languages );
	    if(!empty($languages)){
	    	$lngs = array_reverse( $languages );
	        foreach($lngs as $l) {
        		$active = $l['active'] == 1 ? " active" : "";
        		if(  $l['active'] != 1 ) {
        			return $l;
        		}
	        }
	    }
    }
}

function change_lang()
{
	if( defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE != "" ) 
	{
		$lang_str = ICL_LANGUAGE_CODE == 'hr' ? 'PROMIJENI JEZIK:' : 'Change Language:';
		$languages = icl_get_languages('skip_missing=0&orderby=id&order=ASC');
		if( !empty( $languages ) ) {
			echo ' <ul class="lang-nav">';
				foreach( $languages as $lang ) {
					$active = $lang['active'] == 1 ? " current" : "";
					$language_code = $lang['language_code'];
					$lang_txt = str_replace( 'en' , 'eng', $language_code );
					$lang_txt = str_replace( 'hr' , 'cro', $language_code );
					echo '<li class="' . $lang_txt . '' . $active. '"><span class="icon"></span><a href="' . $lang['url'] . '">'.$lang_txt.'</a></li>';
				}
			echo ' </ul>';
		}
	}
}


function wpml_id( $pid, $element_type = "" ){
	if( defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE != "" ) {
		global $wpdb;

		$elem_string = $element_type != "" ? " AND element_type = '". $element_type ."' " : "";

		$t_name = $wpdb->prefix.'icl_translations';
		$current_lang = ICL_LANGUAGE_CODE;
		$trid =  $wpdb->get_var("SELECT trid FROM $t_name WHERE element_id = $pid $elem_string LIMIT 1");
		$page_id = $wpdb->get_var("SELECT element_id FROM $t_name WHERE trid = $trid AND language_code = '$current_lang' LIMIT 1 ");

		return $page_id;
	} else {
		return $pid;
	}
}

function wpml_id_lang( $pid, $element_type ="post", $lang ="" )
{
	if( $lang == "" ) {
		$lang = ICL_LANGUAGE_CODE;
	}
	if( defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE != "" ) {
		global $wpdb;
		$t_name = $wpdb->prefix.'icl_translations';
		$current_lang = ICL_LANGUAGE_CODE;
		$trid =  $wpdb->get_var("SELECT trid FROM $t_name WHERE element_id = $pid AND element_type LIKE '%{$element_type}%' LIMIT 1 ");
		$page_id = $wpdb->get_var("SELECT element_id FROM $t_name WHERE trid = $trid AND language_code = '{$lang}' LIMIT 1 ");

		return $page_id;
	} else {
		return $pid;
	}
}

// Current lang u principu za oznacvanje <html lang-a>
function set_current_lang($def="en") {
	if( defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE != "" ) :
		echo ICL_LANGUAGE_CODE;
	else:
		echo $def;
	endif;
}


function get_the_excerpt_by_id($post_id, $limit=50) {
  global $post;  
  $save_post = $post;
  $post = get_post($post_id);
  $output = get_the_excerpt();
  $post = $save_post;
  return word_limiter($output, $limit, "..." );
}


// Mičemo nepotrebne stvari iz headera
remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
remove_action( 'wp_head', 'index_rel_link' ); // index link
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version

function strleft($s1, $s2) {
	return substr($s1, 0, strpos($s1, $s2));
}
function current_url(){
    if(!isset($_SERVER['REQUEST_URI'])){
        $serverrequri = $_SERVER['PHP_SELF'];
    }else{
        $serverrequri =    $_SERVER['REQUEST_URI'];
    }
    $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
    $protocol = strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/").$s;
    $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]);
    return $protocol."://".$_SERVER['SERVER_NAME'].$port.$serverrequri;   
}


function hide_front_end_wp_logo()
{
	if( is_user_logged_in() )
	{
		echo '<style type="text/css"> #wp-admin-bar-wp-logo, #wp-admin-bar-themes, #wp-admin-bar-customize, #wp-admin-bar-comments { display:none; } </style>';
	}
}
add_action( 'wp_head', 'hide_front_end_wp_logo' );


function remove_more_link($link) {
	return "";
}
add_filter('the_content_more_link', 'remove_more_link');

remove_shortcode('gallery');
  function gallery_none() {
  return "";
}

add_shortcode('gallery', 'gallery_none' );
function display_gallery() {
	global $post;
	$save_post = $post;
	$post_name = $post->post_name;
	if (strpos($post->post_content,'[gallery') === false){
	  $has_gallery = false;
  	}else{
	  $has_gallery = true;
  	}

  	preg_match('/\[gallery.*ids=.(.*).\]/', $post->post_content, $ids);
	$images = explode(",", $ids[1]);

	$otp = '';
	$q = new WP_Query( array('post_type' => 'attachment', 'post__in' => $images, 'post_status' => 'any', 'posts_per_page' => -1, 'orderby' => 'post__in', ) );
	if( $q->have_posts() ) : 
		$otp .= '<div class="gallery-content no-flick">';
		$otp .=	'<div class="gallery-inner">';
		$otp .=		'<div class="overlay"></div>';
		$otp .=		'<div class="gallery-sl" data-method="frameSlider">';
		while( $q->have_posts() ) : $q->the_post(); 
			$otp .=	wp_get_attachment_image( get_the_ID(), 'gallery-thumb' );
		endwhile;
		$otp .=		'</div>';
		$otp .=	'</div>';
		$otp .= '</div>';
	endif;
wp_reset_postdata();
$post = $save_post;
return $otp;
}



function the_post_ancestor() {
	global $post;
	if( $post->post_parent == 0 ) {
		$result = $post->ID;
	} else {				
		$ancestors 		= get_post_ancestors( $post->ID ); 
		$result  		= array_pop( $ancestors ); 
	}
	return $result;
}  

add_filter( 'wp_mail_from_name', 'my_mail_from_name' );
function my_mail_from_name( $name )
{
    return '';
}


function add_short_box() {
	add_meta_box( 'postexcerpt', __( 'Short text' ), 'post_excerpt_meta_box', 'press', 'normal', 'core' );
	//add_meta_box( 'postexcerpt', __( 'Text' ), 'post_excerpt_meta_box', 'testimonial', 'normal', 'core' );
}
add_action( 'admin_menu', 'add_short_box' );


//add_action('init', 'wpse42279_add_endpoints');
function wpse42279_add_endpoints()
{
    add_rewrite_endpoint('slovo', EP_PAGES);
    add_rewrite_endpoint('letter', EP_PAGES);
}

function is_ajax_call(){
    return isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';
}

add_action( 'wp_head', 'add_no_follow' );
function add_no_follow(){	
	if( contains_substr( 'redneck.media', current_url() ) ) {
		echo "\n\r" . '<meta name="robots" content="nofollow" />'. "\n\r";
	}
}

function change_month_lang($custom_date) {
	$months_eng = array( 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    $months_cro = array( 'siječanja', 'veljače', 'ožujka', 'travnja', 'svibnja', 'lipnja', 'srpnja', 'kolovoza', 'rujna', 'listopada', 'studenog', 'prosinca');
    $newDate = str_ireplace($months_eng, $months_cro, $custom_date);
    return $newDate;
}

// ACF
if( $the_user->ID != 1 ) {
	add_filter('acf/settings/show_admin', '__return_false');
}

function title_span($string) {
	$title_output = preg_replace("/\*(.*?)\*/", "<span>$1</span>", $string);
    return $title_output;
}

function title_span_remove($string) {
	$title_output = preg_replace("/\*(.*?)\*/", "$1", $string);
    return $title_output;
}

function curl_get_contents($url)
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);

    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}

// if( function_exists('acf_add_options_page') ) {
// 	acf_add_options_page(array(
// 		'page_title' 	=> 'Page Info',
// 		'menu_title'	=> 'Page Info',
// 		'menu_slug' 	=> 'page-general-settings',
// 		'capability'	=> 'edit_posts',
// 		'redirect'		=> false
// 	));
	
// 	// acf_add_options_sub_page(array(
// 	// 	'page_title' 	=> 'Children Menu',
// 	// 	'menu_title'	=> 'Children Menu',
// 	// 	'parent_slug'	=> 'page-general-settings',
// 	// ));
// }



// add_filter( 'gform_currencies', 'update_currency' );
// function update_currency( $currencies ) {
//     $currencies['HRK'] = array(
//         'name'               => __( 'HRK', 'gravityforms' ),
//         'symbol_left'        => '',
//         'symbol_right'       => 'kn',
//         'symbol_padding'     => ' ',
//         'thousand_separator' => ',',
//         'decimal_separator'  => '.',
//         'decimals'           => 2
//     );

//     return $currencies;
// }
