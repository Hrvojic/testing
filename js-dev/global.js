
$(document).ready(function(){
	
});


if (window.ADMIN_BAR_VISIBLE) {
    Barba.Pjax.preventCheck = function() {
      return false;
    };
}

function load_reveal() {
  var notMobile = $( window ).width() > 680 ? true : false;
  if(notMobile){
    $('.reveal').waypoint({
        handler: function() {
          var delay_time = $(this.element).data('delay') || 0;
          var element = $(this.element);
              setTimeout(function() {
                  element.addClass("animated");
              }, delay_time);
        },
        offset: function() {
          var elemOffset = $(this.element).data('offset') || 0;
              if (Waypoint.viewportWidth() <= 480) {
                  return Waypoint.viewportHeight() - 180;
              }
              return Waypoint.viewportHeight() + elemOffset;
          }
    });
  } else {
    $('.reveal').addClass('animated');
  }
}

function rellax() {

  var $rellax_element = $('.rellax');

  if (!$rellax_element.length) {
    return;
  }

  if($( window ).width() > 680){
   var rellax = new Rellax('.rellax', {
     speed: -1,
     center: true,
     round: true,
     vertical: true,
     horizontal: false
   });
  }
}

var uaDetect = new UAParser().getResult();
if (uaDetect.browser.name == 'IE' )  {
  $('body').addClass('ie')
}

$(window).on('load', function(){
  $('.loader').fadeOut(400);

  setTimeout(function(){
    $('body').addClass('loaded');
  }, 400);

});

Barba.Pjax.start();
Barba.Prefetch.init();

var FadeTransition = Barba.BaseTransition.extend({
  start: function() {
    /**
     * This function is automatically called as soon the Transition starts
     * this.newContainerLoading is a Promise for the loading of the new container
     * (Barba.js also comes with an handy Promise polyfill!)
     */
    $('html, body').animate({
      scrollTop: 0
    }, 1000, 'swing');
    // As soon the loading is finished and the old page is faded out, let's fade the new page
    Promise
      .all([this.newContainerLoading, this.fadeOut()])
      .then(this.fadeIn.bind(this));
  },

  fadeOut: function() {
    /**
     * this.oldContainer is the HTMLElement of the old Container
     */

      $('body').removeClass('menu-open');
      $('.nav-icon').removeClass('open');

      $('#barba-wrapper').addClass('page-unwrap');
      setTimeout(function(){
         $('#barba-wrapper').addClass('active');
      }, 50);

      // $(this.oldContainer).fadeOut('fast');
      return $(this.oldContainer).animate({ opacity: 0 }, 600).promise();

  },

  fadeIn: function() {
    /**
     * this.newContainer is the HTMLElement of the new Container
     * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
     * Please note, newContainer is available just after newContainerLoading is resolved!
     */
      // $(window).scrollTop(0);



      var _this = this;
      var $el = $(this.newContainer);
      $(this.oldContainer).hide();

      $el.css({
        visibility : 'visible',
        opacity : 0
      });

      setTimeout(function(){
         $('#barba-wrapper').addClass('animated');
      }, 400);
       
      setTimeout(function(){
         $('#barba-wrapper').removeClass('active');
         $('#barba-wrapper').removeClass('animated');
         $('#barba-wrapper').removeClass('page-unwrap');
      }, 800);

      
      $el.addClass('fadeIn').animate({ opacity: 1 }, 600, function() {
        /**
         * Do not forget to call .done() as soon your transition is finished!
         * .done() will automatically remove from the DOM the old Container
         */


      _this.done();
    });
  }

});

/**
 * Next step, you have to tell Barba to use the new Transition
 */

Barba.Pjax.getTransition = function() {
  /**
   * Here you can use your own logic!
   * For example you can use different Transition based on the current page or link...
   */

  return FadeTransition;
};

Barba.Dispatcher.on('transitionCompleted', function(currentStatus, prevStatus) {
    DOM.LoadMethod();
    $(window).trigger('resize');
});

Barba.Dispatcher.on('initStateChange', function() { 
  if (window.ga && document.location.hostname != "localhost") { 
    gtag("event", "page_view", {page_path: window.location.pathname});
    // fbq('init', '123456789');
    // fbq('track', 'PageView');
  } 

});

FastClick.attach(document.body);