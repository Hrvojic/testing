<?php

//adding iis_get_script_map(server_instance, virtual_path, script_extension)
function enqueue_theme_scripts_gallerys() {
    wp_enqueue_script('jquery-ui-sortable');
}

// acctualy hook our scripts and style if we on theme page
if( ( isset($_GET['page']) && $_GET['page'] == 'gallerys-order') ) {
    add_action('admin_print_scripts', 'enqueue_theme_scripts_gallerys');
}


// Order gallerys functionality
add_action( 'admin_menu', 'gallerys_order_menu' );
function gallerys_order_menu() {
	 add_submenu_page('edit.php?post_type=gallery', 'Order of Galleries', 'Order of Galleries', 'manage_options', 'gallerys-order', 'callback_gallery_options');
}

// Callback funciton
function callback_gallery_options() {

	$page_url = admin_url('edit.php?post_type=gallery&page=gallerys-order');
	$term_id = isset($_GET['term_id']) ? $_GET['term_id'] : '';

	if( isset( $_POST['submit_order'] )  ) {
		global $wpdb;
		$other_lang = ICL_LANGUAGE_CODE == 'hr' ? 'en' : 'hr';

		$posts = $_POST['gallery_post'];
		$c = 1;
		foreach( $posts as $id ) {
			$gallery['ID'] = $id;
			$gallery['menu_order'] = $c;

			$post_id   = $id;
			$order_num = $c;

			$wpdb->update( 
				'wp_posts', 
				array( 
					'menu_order' => $order_num
				), 
				array( 'ID' => $post_id )
			);

			$translated_id = wpml_id_lang( $post_id, 'gallery', $other_lang );
			if( $translated_id != NULL ) {
				$wpdb->update( 
				'wp_posts', 
				array( 
					'menu_order' => $order_num
				), 
				array( 'ID' => $translated_id )
			);
			}
		//	wp_update_post( $gallery );
			$c++;
		}

		$updated = true;
	}

?>
<style type="text/css">
	.choose-cat {
		margin-top: 20px;
		margin-bottom: 40px
	}
	#list-of-gallerys li {
		background: #F1F1F1;
		background-image: -webkit-gradient(linear,left bottom,left top,from(#ECECEC),to(#F9F9F9));
		background-image: -webkit-linear-gradient(bottom,#ECECEC,#F9F9F9);
		background-image: -moz-linear-gradient(bottom,#ECECEC,#F9F9F9);
		background-image: -o-linear-gradient(bottom,#ECECEC,#F9F9F9);
		background-image: linear-gradient(to top,#ECECEC,#F9F9F9);
		padding: 10px;
		border-radius: 3px;
		border: 1px solid #ccc;
		font-weight: bold;
		cursor: pointer;
	}
</style>
<div id="wpbody-content" aria-label="Main content" tabindex="0">
	<div class="wrap">
		<div id="icon-options-general" class="icon32"><br></div><h2>Set Order of gallerys</h2>
		<?php if( isset( $updated ) ) : ?>
		    <div id="setting-error-settings_updated" class="updated settings-error below-h2"> 
		    <p><strong>Settings saved.</strong></p></div>
		<?php endif; ?>
		

		<?php 
			add_filter('pre_get_posts','photos_order_by');
			$current_term_id = $term_id != '' ? $term_id : $categories[0]->term_id; 
			$q = new WP_Query(array(
				'post_type' 		=> 'gallery',
				'posts_per_page'	=> '-1',
				'orderby'			=> 'menu_order',
				'order'				=> 'ASC',
				'post_status'		=> 'publish'
			));
		?>

		<?php if( $q->have_posts() ) : ?>
		<form method="post" action="#" >
			<ul id="list-of-gallerys">
				<?php while( $q->have_posts() ) : $q->the_post(); ?>	
					<li>
						<?php the_title(); ?>
						 <input type="hidden" name="gallery_post[]" value="<?php the_ID(); ?>" >
					</li>
				<?php endwhile; ?>
			</ul>
			<?php endif; ?>
			<?php wp_reset_postdata(); ?>
			<p class="submit"><input type="submit" name="submit_order" id="submit_order" class="button button-primary" value="Save Changes"></p></form>
		</form>

	</div>
	<div class="clear"></div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function(){
		 jQuery('#list-of-gallerys').sortable();
		jQuery( '#choose-category' ).change(function(){
			var $this = jQuery(this);
			var newVal = $this.val();

			window.location.href = '<?php echo $page_url ?>' + '&term_id='+newVal;

			console.log( newVal );
		});
	});
</script>
<?php
add_filter('pre_get_posts','photos_order_by');
}
