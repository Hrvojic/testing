<?php 
global $post, $wp_query;
	$original_post = $post;

	wp_enqueue_media();
?>

<style type="text/css">
	.custom-list-table tr td {
		border-bottom: 1px solid #eee;
	}
</style>

	<script type="text/javascript">
	jQuery( document ).ready( function(){  
		jQuery( '.submitdelete-file' ).live('click', function(){
			jQuery(this).closest('tr').remove();
			return false;
		});

		// Uploading files
		var file_frame;

		  jQuery('#add_file').live('click', function( event ){
		 
		    event.preventDefault();
		 
		    // If the media frame already exists, reopen it.
		    if ( file_frame ) {
		      file_frame.open();
		      return;
		    }
		 
		    // Create the media frame.
		    file_frame = wp.media.frames.file_frame = wp.media({
		      title: 'Choose File',
		      
		      button: {
		        text: 'Select File',
		      },
		      multiple: true  // Set to true to allow multiple files to be selected
		    });
		 
		    // When an image is selected, run a callback.
		    file_frame.on( 'select', function() {
		      // We set multiple to false so only get one image from the uploader
		      attachment = file_frame.state().get('selection').first().toJSON();
		 		
		 	
		 	jQuery.ajax({
		          type: "POST",
		          data : { 
		          	action: "get_atatched_file",
		          	attachment_id: attachment.id
		          	//name : name.val(),
		          	//email  : email.val(),
		          },
		          url: "<?php echo admin_url('admin-ajax.php'); ?>",
		          success: function(data) {
		          	if( jQuery('#list-of-files').find( '#attachment-'+attachment.id ).length == 0 ){
			          	jQuery('#list-of-files').append( data );
		          	}
		          }
        	}); 

		      // Do something with attachment.id and/or attachment.url here
		    });
		    // Finally, open the modal
		    file_frame.open();
		  });

	});
	</script>
	<p>Clik Add File button to add file</p>
	<a href="#"  class="button button-primary button-large" id="add_file">Add File</a>
	<?php  $files = explode( ',', get_postmeta_val('attach_file') ); ?>
	<?php  $q = new WP_Query( array( 'post_type' => 'attachment', 'post_status' => 'any', 'post__in' => $files, 'orderby' => 'post__in', 'posts_per_page' => -1 ) ); ?>
	<table class="wp-list-table widefat fixed pages custom-list-table" cellspacing="0" style="margin:20px 0;border-bottom:0;">
	  <tbody id="list-of-files">
	  	<?php if( $q->have_posts() ) : while( $q->have_posts() ) : $q->the_post();  ?>
	  	<?php if( wp_get_attachment_url( get_the_ID() ) ) : ?>
	    <tr id="attachment-<?php the_ID(); ?>" valign="top" style="background:#fff;">
	      <input name="attach_file[]" type="hidden" value="<?php the_ID(); ?>">
	      <td class="post-title page-title column-title">
	        <strong>
	            <?php the_title(); ?>
	        </strong>
	        <div class="row-actions">
	          <span class="edit-file">
	            <a href="<?php echo get_edit_post_link( get_the_ID() ); ?>" title="Edit this item">
	              Edit
	            </a>
	            | 
	          </span>
	          <span class="trash">
	            <a class="submitdelete-file" title="Remove this file" href="#">
	              Remove
	            </a>
	            | 
	          </span>
	          <span class="view-file">
	            <a href="<?php echo $post->guid; ?>" title="View “All about Southwest Florida”" rel="permalink">
	              View
	            </a>
	          </span>
	        </div>
	      </td>
	      
	      <td class="author column-mime-type" style="width:120px">
	        <span>
	          <?php echo strtoupper( pathinfo($post->guid, PATHINFO_EXTENSION) );  ?>
	        </span>
	      </td>
	      <td class="date column-date">
	        <abbr >
	          <?php echo return_date('Y/m/d'); ?>
	        </abbr>
	        <br>
	        <?php echo $post->post_status; ?>
	      </td>
	    </tr>
		<?php endif; // if get_attachment_url ?>
		<?php endwhile; endif; ?>
	  </tbody>
	</table>
	<?php $post = $original_post; wp_reset_query(); wp_reset_postdata(); ?>