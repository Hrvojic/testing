<?php 
	global $post;
?>

<div class="page-pagesub">
	<div id="titlewrap">
	<label class="screen-reader-text" for="page-pagesubtitle">Page Subtitle</label>
	<?php postmeta_textbox('page_pagesubtitle', '', array( 'placeholder' => 'Pagesub', 'style' => 'width:100%;margin-bottom:20px;font-size: 15px;padding:10px 5px;' ) ); ?>
</div>
</div>