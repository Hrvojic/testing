<?php
class GlobalType extends Admin {

	protected static $instance = null;
	// public $post_type = 'apartments';

	/*
		Construstor
	*/
	function __construct()
	{
		// register post types and taxonomies
		//add_action( 'init', array( $this, 'register_post_type' ) );
		//add_action( 'init', array( $this, 'create_taxonomies' ) );

		// load additional scripts and styles
		// add_action( 'admin_enqueue_scripts', array( $this, 'load_extra_scripts' ) );

		// add custom forms and metaboxes
		add_action( 'edit_form_after_title', array( $this, 'subheading' ) );
		add_action( 'edit_form_after_title', array( $this, 'pagesub' ) );

		//save hooks for custom forms & fields
		add_action( 'save_post', array( $this, 'save_subheading' ) );
		add_action( 'save_post', array( $this, 'save_pagesub' ) );

		// load metaboxes
		add_action('add_meta_boxes', array( $this, 'load_mbox' ) );


	}


	/*
	* Save subheading
	* params: $post_id
	*/
	function save_subheading($post_id) {
		if( postmeta_valid( 'page_subheading_noncename', 'page_subheading_noncevlue' ) ) {
			postmeta_save( 'page_subtitle' );
		}
	}

	/*
	* Show subheading in admin
	* params: "";
	*/
	function subheading() {
		global $post;
		$this->load_view( 'subheading' );
		wp_nonce_field('page_subheading_noncevlue', 'page_subheading_noncename');
	}


		/*
	* Save pagesub
	* params: $post_id
	*/
	function save_pagesub($post_id) {
		if( postmeta_valid( 'page_pagesub_noncename', 'page_pagesub_noncevlue' ) ) {
			postmeta_save( 'page_pagesubtitle' );
		}
	}

	/*
	* Show pagesub in admin
	* params: "";
	*/
	function pagesub() {
		global $post;
		$this->load_view( 'pagesub' );
		wp_nonce_field('page_pagesub_noncevlue', 'page_pagesub_noncename');
	}
}
GlobalType::get_instance();
