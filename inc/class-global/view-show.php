<?php 
global $post, $wpdb;
$table_name = $wpdb->prefix . 'post_show';
$p_array = array();
$post_shows = $wpdb->get_results( "SELECT show_id FROM $table_name WHERE post_id = $post->ID" );
if( !empty( $post_shows ) ) {
	foreach( $post_shows as $ps ) {
		$p_array[] = $ps->show_id;
	}
}
?>
<?php $shows = get_posts( array( 'post_type' => 'show', 'posts_per_page' => -1 ) ); 
?>
<table class="form-table">
	<tr>
		<td>
			<?php if( empty( $shows ) ) : ?>
				<p>No shows to attacht the artcile</p>
			<?php else : ?>
			<ul>
				<?php foreach( $shows as $show ) : ?>
					<?php 
						$active = in_array(  $show->ID, $p_array) ? ' checked' : '';
					?>
					<li><input  <?php echo $active; ?> type="checkbox" value="<?php echo $show->ID; ?>" name="show_post[<?php echo $show->ID ?>]" id="show-post-<?php echo $show->ID ?>"><label for="show-post-<?php echo $show->ID ?>"><?php echo $show->post_title; ?></label></li>
				<?php endforeach; ?>
			</ul>
			<?php endif; ?>
		</td>
	</tr>
</table>