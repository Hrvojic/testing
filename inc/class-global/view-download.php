<?php
	global $post;
?>


<table class="form-table">
	<tr>
		<th>Choose file</th>
		<td><?php postmeta_uploader('dw_link'); ?></td>
	</tr>
	<tr>
		<th>Button Text</th>
		<td><?php postmeta_textbox('dw_text', 'Download'); ?></td>
	</tr>
</table>