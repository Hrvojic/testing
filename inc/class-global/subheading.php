<?php 
	global $post;
?>

<div class="page-subheading">
	<div id="titlewrap">
	<label class="screen-reader-text" for="page-subtitle">Subheading</label>
	<?php postmeta_textbox('page_subtitle', '', array( 'placeholder' => 'Subheading', 'style' => 'width:100%;margin-bottom:20px;font-size: 15px;padding:10px 5px;' ) ); ?>
</div>
</div>