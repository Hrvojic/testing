<?php
/**
 *
 * @package WordPress 
 * @subpackage wp_starter
 * Definirane su sve dodatne opcije za temu
 *
**/
function display_option($option,$txt="") {
    if( get_option($option) != "" ) {
        echo get_option($option);
    } else {
        echo $txt;
    }
}
function display_option_ml($option,$txt="") {
    if( get_option($option . '_'.ICL_LANGUAGE_CODE ) != "" ) {
        echo get_option($option .'_'.ICL_LANGUAGE_CODE );
    } else {
        echo $txt;
    }
}
function return_option($option,$txt="") {
    if( get_option($option) != "" ) {
        return get_option($option);
    } else {
        return $txt;
    }
}
function return_option_ml($option,$txt="") {
    if( get_option( $option .'_'.ICL_LANGUAGE_CODE ) != "" ) {
        return get_option( $option .'_'.ICL_LANGUAGE_CODE );
    } else {
        return $txt;
    }
}

function get_option_val_tag($option, $tag_open="", $tag_close="" ) {
    if( get_option($option) != "" ) {
        return $tag_open .  get_option($option) . $tag_close ;
    } 
}

function get_postmeta_val_tag($option, $tag_open="", $tag_close="" ) {
    if( get_postmeta_val($option) != "" ) {
        return $tag_open .  get_postmeta_val($option) . $tag_close ;
    } 
}

// adding styles 
function enqueue_theme_styles() {
    wp_enqueue_style('thickbox');
  //  wp_enqueue_style( 'header-theme-options', get_template_directory_uri() . '/inc/theme-options.css', false, '2012-02-10' );
}

//adding iis_get_script_map(server_instance, virtual_path, script_extension)
function enqueue_theme_scripts() {
    wp_enqueue_script('tiny_mce');
    wp_enqueue_script('media-upload');
    //wp_enqueue_script('thickbox');
   // wp_enqueue_script('header-theme-script', get_template_directory_uri() . '/inc/theme-options.js', false, '2012-02-10');
}

// acctualy hook our scripts and style if we on theme page
if( ( isset($_GET['page']) && $_GET['page'] == 'theme-options') ) {
    add_action('admin_print_styles', 'enqueue_theme_styles');
    add_action('admin_print_scripts', 'enqueue_theme_scripts');
}

add_action('admin_menu', 'contact_option_menu');



//addign submenu
function contact_option_menu() {
    add_menu_page( 'Site Options', 'Site Options', 'manage_options', 'theme-options', 'callback_theme_options');
    //add_submenu_page('themes.php', 'Site Info', 'Site Info', 'manage_options', 'theme-options', 'callback_theme_options');
    add_action('admin_init', 'register_theme_options');
}

function register_theme_options() {
    register_setting( 'theme-options-group', 'facebook' );
    register_setting( 'theme-options-group', 'twitter' );
    register_setting( 'theme-options-group', 'linkedin' );
    register_setting( 'theme-options-group', 'instagram' );
    register_setting( 'theme-options-group', 'pinterest' );
    register_setting( 'theme-options-group', 'youtube' );

    register_setting( 'theme-options-group', 'google_analytics' );
    register_setting( 'theme-options-group', 'main_email' );

    register_setting( 'theme-options-group', 'location' );
    register_setting( 'theme-options-group', 'number_mb' );
    register_setting( 'theme-options-group', 'main_tel' );
    register_setting( 'theme-options-group', 'number_oib' );
    register_setting( 'theme-options-group', 'number_bank' );
    register_setting( 'theme-options-group', 'main_tel' );
    register_setting( 'theme-options-group', 'main_fax' );
    
    register_setting( 'theme-options-group', 'production_development' );
    register_setting( 'theme-options-group', 'show_cookie' );
    // register_setting( 'theme-options-group', 'company_address_'.ICL_LANGUAGE_CODE );

}

function callback_theme_options() {
?>
<div class="wrap" style="padding:20px">
<?php screen_icon(); ?>
<h2><?php _e('Site information'); ?></h2>
<?php settings_errors(); ?>
<br><br>
<form method="post" action="options.php">
    <?php settings_fields( 'theme-options-group' ); ?>

    <div class="theme-box">
        

        <div class="open-closed">
            <h3>Production/Development Status</h3>
            <?php $production_development = get_option( 'production_development' ); ?>
            Development
            <input type="radio" name="production_development" value="0"<?php checked( '0' == $production_development ); ?> />
            Production
            <input type="radio" name="production_development" value="1"<?php checked( '1' == $production_development ); ?> />
            <p>
            <label for="<?php echo 'google_analytics' ?>">Google Analytics ID</label><br>
            <input style="width:500px;padding:7px" type="text" name="<?php echo 'google_analytics' ?>" id="<?php echo 'google_analytics' ?>" value="<?php echo get_option( 'google_analytics' ); ?>" placeholder="UA-XXXXXXXXX-X">
            </p>
            <label for="<?php echo 'show_cookie' ?>">Show Cookie Notice</label><br>
            <?php $show_cookie = get_option( 'show_cookie' ); ?>
            No
            <input type="radio" name="show_cookie" value="0"<?php checked( '0' == $show_cookie ); ?> />
            Yes
            <input type="radio" name="show_cookie" value="1"<?php checked( '1' == $show_cookie ); ?> />
        </div>


        <div class="company_networks">
            <h2>Contact info</h2>
            <p>
            <label for="<?php echo 'location' ?>">Location</label><br>
            <textarea name="<?php echo 'location' ?>" id="<?php echo 'location' ?>" cols="30" rows="10" style="width: 80%;"><?php echo get_option( 'location' ); ?></textarea>
            </p>
            <p>
            <label for="<?php echo 'main_email' ?>">E-mail</label><br>
            <input style="width:500px;padding:7px" type="text" name="<?php echo 'main_email' ?>" id="<?php echo 'main_email' ?>" value="<?php echo get_option( 'main_email' ); ?>">
            </p>
            <p>
            <label for="<?php echo 'number_mb' ?>">MB</label><br>
            <input style="width:500px;padding:7px" type="text" name="<?php echo 'number_mb' ?>" id="<?php echo 'number_mb' ?>" value="<?php echo get_option( 'number_mb' ); ?>">
            </p>
            <p>
            <label for="<?php echo 'number_oib' ?>">OIB</label><br>
            <input style="width:500px;padding:7px" type="text" name="<?php echo 'number_oib' ?>" id="<?php echo 'number_oib' ?>" value="<?php echo get_option( 'number_oib' ); ?>">
            </p>
            <p>
            <label for="<?php echo 'number_bank' ?>">Račun</label><br>
            <input style="width:500px;padding:7px" type="text" name="<?php echo 'number_bank' ?>" id="<?php echo 'number_bank' ?>" value="<?php echo get_option( 'number_bank' ); ?>">
            </p>
            <p>
            <label for="<?php echo 'main_tel' ?>">Tel.</label><br>
            <input style="width:500px;padding:7px" type="text" name="<?php echo 'main_tel' ?>" id="<?php echo 'main_tel' ?>" value="<?php echo get_option( 'main_tel' ); ?>">
            </p>
            <p>
            <label for="<?php echo 'main_fax' ?>">Fax.</label><br>
            <input style="width:500px;padding:7px" type="text" name="<?php echo 'main_fax' ?>" id="<?php echo 'main_fax' ?>" value="<?php echo get_option( 'main_fax' ); ?>">
            </p>
        </div>    

        <div class="company_networks">
            <h2>Social Networks</h2>
            <p>
                <label for="linkedin">LinkedIn</label><br>
                <input style="width:300px;" type="text" name="linkedin" id="linkedin" value="<?php echo get_option('linkedin'); ?>">
            </p>
            <p>
                <label for="facebook">Facebook</label><br>
                <input style="width:300px;" type="text" name="facebook" id="facebook" value="<?php echo get_option('facebook'); ?>">
            </p>
             <p>
                <label for="twitter">Twitter</label><br>
                <input style="width:300px;" type="text" name="twitter" id="twitter" value="<?php echo get_option('twitter'); ?>">
            </p>
            <p>
                <label for="youtube">Youtube</label><br>
                <input style="width:300px;" type="text" name="youtube" id="youtube" value="<?php echo get_option('youtube'); ?>">
            </p>
            <p>
                <label for="instagram">Instagram</label><br>
                <input style="width:300px;" type="text" name="instagram" id="instagram" value="<?php echo get_option('instagram'); ?>">
            </p>
            <p>
                <label for="pinterest">Pinterest</label><br>
                <input style="width:300px;" type="text" name="pinterest" id="pinterest" value="<?php echo get_option('pinterest'); ?>">
            </p>
        </div>

    </div>

    

    <p class="submit">
        <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
    </p>
</form>

<?php
}

/*END SUBHEADER OPTIONS   */