<?php 
$style_minified = get_template_directory_uri().'/css/style.css';
if (file_exists($style_minified)):
?>

<script>
  function loadCss(href){
    var ss = window.document.createElement('link'),
        ref = window.document.getElementsByTagName('head')[0];
    ss.rel = 'stylesheet';
    ss.href = href;
    ss.media = 'only x';
    ref.parentNode.insertBefore(ss, ref);
    setTimeout( function(){
      ss.media = 'all';
    },0);
  }
  loadCss('<?php echo $style_minified; ?>');
</script>
<noscript>
  <link rel="stylesheet" href="<?php echo $style_minified; ?>">
</noscript>

<?php endif; ?>