<script>
    window.cookieconsent_options = {
        learnMore: 'more info',
        theme: '<?php echo get_template_directory_uri(); ?>/inc/theme-addons/cookie-notice/cookie-notice.css'
    }
</script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/inc/theme-addons/cookie-notice/cookie-notice.js"></script>