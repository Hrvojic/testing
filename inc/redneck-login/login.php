<?php  
/* REDNECK custom login options */
function enqueue_login_styles()
{
	wp_enqueue_style('redneck-login', get_template_directory_uri() . '/inc/redneck-login/login-style.css', false, '1'); 
}
add_action( 'login_enqueue_scripts', 'enqueue_login_styles' );

// Cutom URL for login page
add_filter( 'login_headerurl', 'redneck_login_url' );
function redneck_login_url($url) {
	return home_url();
}