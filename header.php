﻿<?php
/**
 * @package WordPress
 * @subpackage wp_starter
 * @since v1.0
 **/
?>
<!DOCTYPE html>
<!--[if IE 7 ]><html lang="hr" class="no-js ie ie7"> <![endif]-->
<!--[if IE 8 ]><html lang="hr" class="no-js ie ie8"> <![endif]-->
<!--[if IE 9 ]><html lang="hr" class="no-js ie ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="hr" class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title><?php wp_title( '|', true, 'right' ); ?></title>

    <link id="style" href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet" type="text/css">
    <link id="style" href="<?php echo get_template_directory_uri(); ?>/css/wp-style.css" rel="stylesheet" type="text/css">

    <?php 
        // Use https://realfavicongenerator.net - place generated favicons in wp-starter/favicon/ folder
        get_template_part( 'inc/theme-addons/favicon' ); 
    ?>
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <script type="text/javascript">
        var ADMIN_BAR_VISIBLE = false;
        var admin_url = '<?php echo admin_url( "admin-ajax.php" ); ?>';
    </script>
    <?php if( is_admin_bar_showing() ): ?>
    <script type="text/javascript">
        var ADMIN_BAR_VISIBLE = true;
    </script>
    <?php endif; ?>

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<!-- <div class="loader">
    <img src="<?php echo get_template_directory_uri() ?>/img/icons/logo.png" alt="<?php echo bloginfo( 'name' ); ?>">
</div>

<div class="mobile-menu-wrapper">
    <nav class="outer-nav right vertical">
        <?php wp_nav_menu( Navigation::menu_args('mobile_menu') ); ?>
    </nav>
</div> -->

<!-- Header -->
<!-- <header id="page-header" role="banner">
        <h1 class="site-logo"><a href="<?php echo home_url(); ?>" title="<?php echo bloginfo( 'name' ); ?>"><?php echo bloginfo('name'); ?></a></h1>           
        <nav>
            <?php wp_nav_menu( Navigation::menu_args('primary_menu') ); ?>
        </nav>
</header> -->
<!-- /Header -->
testing


<div id="page-wrapper">
<div id="barba-wrapper">
<div <?php body_class('barba-container'); ?> data-namespace="page">