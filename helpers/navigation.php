<?php

/**
 * @package WordPress
 * @subpackage wp_starter
 * @since v1.0
 * sadržava vecinu pomocnih funkcija vezanih za navigaciju kroz stranicu
 **/

function breadcrumbs($home="Početna", $delimiter="<span> / </span>") {
  $home = ICL_LANGUAGE_CODE == 'hr' ? "Početna" : 'Home';
  $before = ''; // tag before the current crumb
  $after = ''; // tag after the current crumb
 
  if ( !is_home() && !is_front_page() || is_paged() ) {
 
 //   echo '<ol id="breadcrumbs" class="container">';
 	
    global $post;
    $homeLink = get_bloginfo('url');
    echo $before.'<a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . $after;
 
    if ( is_category() ) {
      global $wp_query;
      $cat_obj = $wp_query->get_queried_object();
      $thisCat = $cat_obj->term_id;
      $thisCat = get_category($thisCat);
      $parentCat = get_category($thisCat->parent);
      if ($thisCat->parent != 0) echo $before."" . (get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' ')) . $after;
      echo $before . '' . single_cat_title('', false) . '' . $after;
 
    } elseif ( is_day() ) {
      echo $before.'<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . $after;
      echo $before.' <a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . $after;
      echo $before . get_the_time('d') . $after;
 
    } elseif ( is_month() ) {
      echo $before.' <a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . $after;
      echo $before . get_the_time('F') . $after;
 
    } elseif ( is_year() ) {
      echo $before . get_the_time('Y') . $after;
 
    } elseif ( is_single() && !is_attachment() ) {
      if ( get_post_type() != 'post' ) {
        if( get_post_type() == 'member' ) {
            $the_page = get_post( wpml_id( 36 ) );
            $post_type = get_post_type_object(get_post_type());
            $slug = $post_type->rewrite;
            echo $before.' <a href="'. get_permalink( wpml_id( 36 )  ) .'"">' . $the_page->post_title . '</a> ' . $delimiter . $after;
            echo $before . get_the_title() . $after;
        } else {
            $post_type = get_post_type_object(get_post_type());
            $slug = $post_type->rewrite;
            echo $before.' <a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a> ' . $delimiter . $after;
            echo $before . get_the_title() . $after;
        }

      } else {
        $cat = get_the_category(); $cat = $cat[0];
        echo $before.'' . get_category_parents($cat, TRUE, '' . $delimiter . '') . $after;
        echo $before . get_the_title() . $after;
      }
 
    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
      $post_type = get_post_type_object(get_post_type());
      echo $before . $post_type->labels->singular_name . $after;
 
    } elseif ( is_attachment() ) {
      $parent = get_post($post->post_parent);
      $cat = get_the_category($parent->ID); $cat = $cat[0];
      echo $before.'' . get_category_parents($cat, TRUE, '' . $delimiter . ' ') . $after;
      echo $before.' <a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a> ' . $delimiter . $after;
      echo $before . get_the_title() . $after;
 
    } elseif ( is_page() && !$post->post_parent ) {
      echo $before . get_the_title() . $after;
 
    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = $before.'<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>'.$after;
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      foreach ($breadcrumbs as $crumb) echo $before."". $crumb . ' ' . $delimiter . $after;
      echo $before . get_the_title() . $after;
 
    } elseif ( is_search() ) {
      echo $before . '' . get_search_query() . '' . $after;
 
    } elseif ( is_tag() ) {
      echo $before . '' . single_tag_title('', false) . '' . $after;
 
    } elseif ( is_author() ) {
     $author_bb = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author')); 
      echo $before . '' . $author_bb->display_name . $after;
 
    } elseif ( is_404() ) {
      echo $before . __('Greška 404') . $after;
    }
 
   /* if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo '(';
      echo __('Stranoca') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }*/
 
  //  echo '</ol>';
 
  }
} // end dimox_breadcrumbs()

function num_pagination($pages = '', $range = 4)
{  
  //koristena odlična kriesi paginacija ali promjenjena kako bi odgovarala strukturi.
     $showitems = ($range * 2)+1;  

     

     global $paged, $wp_query;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   

     if(1 != $pages)
     {
        echo '<div class="pagination">';
        echo '<ul>';
        echo '<li class="prev">'.get_previous_posts_link( __( '&larr; Prethodna', 'twentyeleven' ) ).'</li>';
         for ($i=1; $i <= $pages; $i++)
         {
           $first = $i == 1 ? ' class="first"' : "";
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                // echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
                 if( $paged == $i ) {
                  echo '<li'.$first.'><a href="'.get_pagenum_link($i).'" class="selected" >'.$i.'</a></li>';
                 } else {
                  echo '<li'.$first.'><a href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
                 }
             }
         }
         echo '<li class="next">'.get_next_posts_link( __( 'Sljedeća &rarr;', 'twentyeleven' ) ).'</li>';
         echo '</ul>';
?>
<?php
         echo "</div>\n";
     }
}