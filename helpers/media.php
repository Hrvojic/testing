<?php
/**
 * @package WordPress
 * @subpackage wp_starter
 * @since v1.0
 * sadržava vecinu pomocnih funkcija vezanih za slike
 **/

function foo($atts) 
{
	global $post;

	extract( shortcode_atts( array(
		'foo' => 'something',
		'bar' => 'something else',
	), $atts ) );

	return $bar;
}
add_shortcode('foo', 'foo');
// Custom  Galerija 
function custom_gallery($atts) 
{
	global $post;

	extract( shortcode_atts( array(
		'id' => $post->ID,
	), $atts ) );

	$args = array(
		'post_type' => 'attachment',
		'post_mime_type' => 'image',
		'numberposts' => -1,
		'orderby' => 'menu_order',
		'order'=> 'ACS',
		'post_parent' => $id
	);

	$images = get_posts($args);

	//$container_before = '<ul>';
	if( $images ) : 
	$html = '<div class="gallery cf">';
	$html .= '<h2>'.__('Fotogalerija', 'wp_starter').'</h2>';
	$html .=  '<ul>';
		foreach( $images as $image ) :

		$image_data = wp_get_attachment_image_src( $image->ID, $size='full' );
			
			$html .= '<li><a href="'. $image_data[0] .'"  rel="photo_group" title="'.$image->post_title.'">';
			$html .=  wp_get_attachment_image( $image->ID, $size ='product-thumb');
			$html .= '</a></li>';
		endforeach;
		
	$html .= '</ul>';
	$html .= '</div>';	
	return $html;

	else :
		print_r($atts);
		return $atts['foo']. ' <div class="gallery_empty">Your gallery is empty.</div>';

	endif;
		
}

// remove_shortcode('gallery');
// add_shortcode('gallery', 'custom_gallery');

//dodavanje velicine u media uploader ali eksplicinto
add_filter( 'image_size_names_choose', 'custom_image_sizes_choose' );  
function custom_image_sizes_choose( $sizes ) {  
    $custom_sizes = array(  
        'article-thumb' => 'Article Thumbnail'
    );  
    return array_merge( $sizes, $custom_sizes );  
}  

// uzimamo samo src od thumba
function get_thumbnail_src($size="thumbnail"){
	global $post;
	$thumbnail_id = get_post_thumbnail_id($post->ID);  
	$thumb = wp_get_attachment_image_src( $thumbnail_id, $size );
	return ( isset( $thumb[0] ) && $thumb[0] ) != '' ? $thumb[0] : '';
}

function get_thumbnail_src_by_id( $post_id , $size="thumbnail"){
	$thumbnail_id = get_post_thumbnail_id( $post_id );  
	$thumb = wp_get_attachment_image_src( $thumbnail_id, $size );
	return $thumb[0];
}

//Uzimamo samo prvu sliku iz posta uzimamo 
function catch_first_image() {
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  $first_img = $matches[1][0];

  if(empty($first_img)){ //Defines a default image
    $first_img = "default.jpg";
  }
  return $first_img;
}


// Uzimamo prvu "attached" sliku iz posta
function first_img($size="thumbnail", $echo=true) {
  global $post;
  $files = get_children('post_parent='.get_the_ID().'&post_type=attachment&post_mime_type=image');
  if($files) :
    $keys = array_reverse(array_keys($files));
    $num = $keys[0];
    $image_attributes = wp_get_attachment_image_src( $num, $size );
    $result = '<img src="'.$image_attributes[0].'" width="'.$image_attributes[1].'" height="'.$image_attributes[2].'" class="firstimg" />';
    if( $echo == true ) {
    	echo $result;
    } else {
    	return $result;
    }
  else:
  	return "";
  endif;
}

function first_img_($size="thumbnail", $post_id, $echo=true) {
  $files = get_children('post_parent='.$post_id.'&post_type=attachment&post_mime_type=image');
  if($files) :
    $keys = array_reverse(array_keys($files));
    $num = $keys[0];
    $image_attributes = wp_get_attachment_image_src( $num, $size );
    $result = '<img src="'.$image_attributes[0].'" width="'.$image_attributes[1].'" height="'.$image_attributes[2].'" class="firstimg" />';
    if( $echo == true ) {
    	echo $result;
    } else {
    	return $result;
    }
  else:
  	return "";
  endif;
}

// Ispitujemo postoji li barem jedna slika nase velicine u postu.
function has_post_image( $size="thumbnail" ) {
	global $post;
	$original_post = get_post_passed_url_wmpl($post->ID);
	$pid = $original_post != "" ? $original_post : $post->ID;
	if( has_post_thumbnail($pid) ) {
		$result = get_the_post_thumbnail($pid, $size);
	} else {
		$result = first_img_($size, $pid, false );
	}
	if(!empty($result)) { return true; } else { return false; }
}

// First imgage form gallery
function first_img_form_gallery( $size ) {
	global $post;
	$post_name = $post->post_name;
	if (strpos($post->post_content,'[gallery') === false){
	  $has_gallery = false;
  	}else{
	  $has_gallery = true;
  	}


  	preg_match('/\[gallery.*ids=.(.*).\]/', $post->post_content, $ids);
	$images = explode(",", $ids[1]);

	$first_img = array_shift($images);
	
	$result = wp_get_attachment_image_src( $first_img, $size );
	return '<img src="'. $result[0] .'" alt="" width="'. $result[1] .'" height="'. $result[2] .'">';
}

// Prikazujemo neku sliku ako postoji
function the_post_image($size="thumb", $echo=true, $id=""){
	global $post, $_wp_additional_image_sizes;
	global $post;

	$original_post = get_post_passed_url_wmpl($post->ID);
	$pid = $original_post != "" ? $original_post : $post->ID;
	if( has_post_thumbnail($pid) ) {
		$result = get_the_post_thumbnail($pid, $size);
	} else {
		$result = first_img_($size,$pid, false );
	}

	$result = preg_replace( '/(width|height)=\"\d*\"\s/', '', $result );

	if( $echo == true ) {
		echo $result;
	} else {
		return $result;
	}
}

function get_the_post_img_by_id($size="thumb",$id, $echo=true){

	global  $_wp_additional_image_sizes;
	

	if( has_post_thumbnail($id) ) {
		$result = get_the_post_thumbnail($id, $size);
	} else {
		$result = first_img_($size,$id, false );
	}

	if( trim( $result == "" ) ) {

		$post_content = get_page($id);
		$post_data 	  = $post_content->post_content;		

		 $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post_data , $matches);
  		 $img_url = $matches[1][0];

  		 if( trim( $img_url ) != "" ) {

	  		$image_name = pathinfo( $img_url, PATHINFO_FILENAME );
	  		$img_ext = pathinfo( $img_url, PATHINFO_EXTENSION );
	  		$dirname = pathinfo( $img_url, PATHINFO_DIRNAME );

	  		list($i_width, $i_height, $i_ype, $i_attr) = getimagesize( $img_url) ;
	  		$err = array();

	  		$our_size = $_wp_additional_image_sizes[$size];
	  		$tsize = "-".$our_size['width']."x".$our_size['height'];

	  		//$result = $tsize; 
	  		foreach( $_wp_additional_image_sizes as $size ){ //-200x90
	  			$the_size = "-".$size['width']."x".$size['height'];
	  			$r_size   = "-".$i_width."x".$i_height;

	  			$new_img_name = str_replace($the_size, "", $image_name );
	  			$new_img_name =  str_replace($r_size, "", $new_img_name );
	  		}

	  		$new_img_url = $dirname."/".$new_img_name.$tsize.".".$img_ext;
	  		$size_of_img = getimagesize($img_url);

	  		$size_of_img 	= $size_of_img[3];
	  		$thumbnail_size = 'width="'.$our_size['width'].'" height="'.$our_size['height'].'"';

	  		if( $thumbnail_size == $size_of_img ){
	  			$new_img_url = $dirname."/".$new_img_name.".".$img_ext;
	  		} else {
	  			$new_img_url = $dirname."/".$new_img_name.$tsize.".".$img_ext;
	  		}

	  		$result = '<img src="'.$new_img_url.'" alt="">';  	
	  	}
	 	
	}


	if( $echo == true ) {
		echo $result;
	} else {
		return $result;
	}

}


function the_post_image_test($size="thumb", $echo=true){
	global $post, $_wp_additional_image_sizes;
	if( has_post_thumbnail($post->ID) ) {
		$result = get_the_post_thumbnail($post->ID, $size);
	} else {
		$result = first_img($size, false );
	}

	$result = preg_replace( '/(width|height)=\"\d*\"\s/', '', $result );

	if( $echo == true ) {
		return $result;
	} else {
		return $result;
	}
}

function wp_list_attachments() {
	global $post;
	$files = explode( ',', get_postmeta_val('attach_file') ); 
		$q = new WP_Query( array( 'post_type' => 'attachment', 'post_status' => 'any', 'post__in' => $files, 'orderby' => 'post__in' )  );
	?>
	<?php if( $q->have_posts() ) : ?>
		<ul class="big-list">
			<?php while( $q->have_posts() ) : $q->the_post(); global $post; ?>
				<li class="<?php echo pathinfo($post->guid, PATHINFO_EXTENSION); ?>-type"><a href="<?php echo $post->guid; ?>" target="_blank"><i class="icon"></i><?php the_title(); ?> <span class="meta">[<?php echo ceil( filesize( get_attached_file( $post->ID ) ) / 1024  ); ?>kb]</span></a></li>
			<?php endwhile; ?>
		</ul>
	<?php endif; wp_reset_postdata(); 
}

