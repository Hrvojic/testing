<?php
/**
 * @package WordPress
 * @subpackage wp_starter
 * @since v1.0
 * sadržava vecinu pomocnih funkcija vezanih za statistiku bloga
 **/

// Pomoćne funkcije i varijable
global $wpdb;
$popular_posts_table = $wpdb->prefix.'popular_posts';
define('POPULAR_POSTS_TABLE', $popular_posts_table);
define('POSTS_TABLE', $wpdb->prefix.'posts');

//helper metoda -- pogledati get_popular_posts 
function implode_types( $array, $before=" post_type = '", $after="' OR " )
{
	$array_num = count($array);
	if( $array_num > 0 ) :
		$result_array = array();
		$c = 1;
		foreach( $array as $arr ) : 
			if( $c == $array_num ) :
				$after = "'";
			endif;
			
			$result_array[] = $before . $arr . $after;
			
			$c++;
		endforeach;

		$result = implode(" ", $result_array );
	else :
		$result = "post_type = 'post'";
	endif;

	return $result;
}

// obicna pomocna funckijna --- kasnije se brise
function popular_posts_table_name(){
	global $wpdb;
	return $wpdb->prefix.'popular_posts';
}

// Tablica za vodenje statistike najpopularnijih postova 
// 1) Prvo provjeravamo postoji li tablica za popularne postove
if( !table_exists( $popular_posts_table ) ) :

	$query = $wpdb->prepare("CREATE TABLE $popular_posts_table (
			id int(11) NOT NULL AUTO_INCREMENT,
			post_id int(11) NOT NULL,
			today_date  DATETIME NOT NULL,
			today_views int(11) NOT NULL,
			week_date  DATETIME NOT NULL,
			this_week_views int(11) NOT NULL,
			month_date DATETIME NOT NULL,
			this_month_views int(11) NOT NULL,
			last_viewed_at  DATETIME NOT NULL,
			total_views  int(11) NOT NULL,
			lang VARCHAR(255) NOT NULL,
 			UNIQUE KEY id (id)
	);" );

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $query );

endif;

// 2) Kreiramo counter koji ce brojati posjecenost pojedinog posta
function popular_posts_counter(){
	global $post, $wpdb;
	$t_name = POPULAR_POSTS_TABLE;
	//sekunde cemo preskociti za sada...pustaamo za novu verziju...
	$now  = gmdate('Y-m-d H:i:s');
	$date_now = gmdate('Y-m-d 00:00:00');
	$post_id = $post->ID;

	// Gledamo jeli osposobljen WPML
	$post_language = defined( 'ICL_LANGUAGE_CODE' ) && ICL_LANGUAGE_CODE != "" ? ICL_LANGUAGE_CODE : "";
  
  	if( is_singular() ) {

	    $post_exists =  $wpdb->get_row("SELECT * FROM $t_name WHERE post_id = $post_id" );
	    if( $post_exists == null )  {

	      // Ako ne postoji ovaj post onda ga spremamo u bazu sa nekim defoltnim postavkama

	      $wpdb->insert( $t_name, array( 
	      	'post_id'=> $post_id, 
	      	'last_viewed_at' 	=> $now,
	      	'today_date'     	=> $date_now,
	      	'today_views' 	 	=> 1,
	      	'week_date'		 	=> $date_now,
	      	'this_week_views'  	=> 1,
	      	'month_date'       	=> $date_now,
	      	'this_month_views' 	=> 1,
	      	'total_views'    	=> 1,
	      	'lang'				=> "$post_language"
	     
	      )  );
	  
	    }   else  {
	      //Ako vec postoji u bazi onda cemo samo updjetatai postojece podatke

	      // Total views se jednostavno incrementa! Zelimo imati uvid totalnu posjecenost clanka :)
	      $total_views = $post_exists->total_views + 1;
	      // Broj posjeta za danasnji dan
	      $today_views = $post_exists->today_views + 1;
	      // Broj posjeta za ovaj tjedan
	      $this_week_views = $post_exists->this_week_views + 1;
	      //Broj posjeta za ovaj mjesec
	      $this_month_views = $post_exists->this_month_views + 1;

	      // **********************************************************
	      // Sređujemo resetiranje countera za danas, tjedan  i mjesec 
	      // **********************************************************
	      $post_today_date 		 = $post_exists->today_date;
	      $post_this_week_date   = $post_exists->week_date;
	      $post_this_month_date  = $post_exists->month_date;

	      // Ukoliko nije danasnji dan resetiramo counter na "1" i stavljamo na danasnji datum....
	      $post_today_views = ( $post_today_date != $date_now ) ? 1 : $today_views;
	      $new_today_date   = ( $post_today_date != $date_now ) ? $date_now : $post_today_date;

	      // Ukoliko je proslo tjedan dana od kad smo stavili week_date resetiramo counter na "1" i stavljamo danasnji datum
	      $calc_week_from_setdate 	= strtotime( $post_this_week_date ) +  (7 * 24 * 60 * 60);
	      $week_from_setdate 		= date( 'Y-m-d 00:00:00', $calc_week_from_setdate );
	      $post_this_week_views 	= ( $date_now > $week_from_setdate ) ? 1 : $this_week_views;
	      $new_week_date 			= ( $date_now > $week_from_setdate ) ? $date_now : $post_this_week_date;

	      // Ukoliko je proslo mjesec dana od kad smo stavili week_date resetiramo counter na "1" i stavljamo danasnji datum
	      $calc_month_from_setdate 	= strtotime( date( "Y-m-d 00:00:00", strtotime( $post_this_month_date ) ) . "+1 month");
	      $month_from_setdate 		= date( 'Y-m-d 00:00:00', $calc_month_from_setdate );
	      $post_this_month_views    = ( $date_now > $month_from_setdate ) ? 1 : $this_month_views;
	      $new_month_date 			= ( $date_now > $month_from_setdate ) ? $date_now : $post_this_month_date;
	      
	      // Updajetamo sve
	      $wpdb->update($t_name, 
	          array(
	            'total_views' 		=> $total_views,
	            'last_viewed_at' 	=> $now,
	            'today_views'       => $post_today_views,
	            'today_date'		=> $new_today_date,
	            'this_week_views'   => $post_this_week_views,
	            'week_date'			=> $new_week_date,
	            'this_month_views'  => $post_this_month_views,
	            'month_date' 		=> $new_month_date
	          ),
	          array(
	            'post_id' => $post_id
	          )
	        ); 
	    }
  	}

}
add_action('wp_head', 'popular_posts_counter');

// 3) Funckija koja poziva popularne postove
function get_popular_posts_( $number = 5, $time="", $post_types='' )
{
	global $wpdb;
	$t_name = POPULAR_POSTS_TABLE;




	$today 		= date('Y-m-d 00:00:00', strtotime('-24 hours'));
	$week_ago 	= date('Y-m-d 00:00:00', strtotime('-7 days'));
	$month_ago 	= date('Y-m-d 00:00:00', strtotime('-1 month'));

	// 	$time = "AND viewed_at > '$start_of_the_week'";
	if( $time == 'today' ) {
		$time_ago = "AND t2.viewed_at > '$today'";
	} elseif( $time == 'week' ){
		$time_ago = "AND t2.viewed_at > '$week_ago'";
	}elseif( $time == 'month' ) {
		$time_ago = "AND t2.viewed_at > '$month_ago'";
	} else {
		$time_ago = "";
	}


	$posts_table = $wpdb->prefix . 'posts';		
	if( !is_array( $post_types ) && $post_types != '' ) :
		$result =  '<p style="color:red">' . __('<strong>Error from get_popular_posts:</strong> third argument must be an array of post types.') . '</p>';
	else :
		$post_types = $post_types == '' ? $post_types = array( 'post' ) : $post_types;
	    $sel_type = implode_types( $post_types );
	    $result = $wpdb->get_results(
				"
				SELECT * FROM $posts_table AS t1
				INNER JOIN $t_name AS t2
				ON t1.ID = t2.post_id
				WHERE t1.post_status = 'publish'
				$time_ago
				AND $sel_type
				ORDER BY t2.views DESC
				LIMIT $number
				"
		);
	endif;
	
	return $result;
}

function get_popular_posts( $lim = 5, $time="", $post_type='post' ) {
	global $wpdb;
	$t_name = POPULAR_POSTS_TABLE;
	$posts_table = POSTS_TABLE;

	// Ako koristimo WPML onda cemo prikazivati i prema jeziku, a ne sve
	$post_language = defined('ICL_LANGUAGE_CODE') ? ICL_LANGUAGE_CODE : "";
	if( $post_language != "" ) {
		$lang = "AND lang = '$post_language'";
		$lang_only = "WHERE lang = '$post_language'";
	} else {
		$lang = "";
		$lang_only = "";
	}

	$today 		= date('Y-m-d 00:00:00');
	$week_ago 	= date('Y-m-d 00:00:00', strtotime('-7 days'));
	$month_ago 	= date('Y-m-d 00:00:00', strtotime('-1 month'));

	$limit = "LIMIT ".$lim;

	// Ako su navedeni post tipovi napraviti cemo implode tih tipove i spojitu u strig "AND post_type='foo' AND post_type=''foo2..."

	if( $time == "today" ) {
		$result = $wpdb->get_results("SELECT * FROM $t_name INNER JOIN $posts_table
									  ON $t_name.post_id = $posts_table.id
									  WHERE $t_name.today_date = '$today' $lang AND $posts_table.post_type = '$post_type' ORDER BY $t_name.today_views DESC $limit;" );
	} elseif( $time = "week" ) {
		$result = $wpdb->get_results("SELECT * FROM $t_name INNER JOIN $posts_table
									  ON $t_name.post_id = $posts_table.id
									  WHERE ($t_name.week_date > '$week_ago' OR
									  $t_name.week_date = '$week_ago') $lang AND $posts_table.post_type = '$post_type'
									  ORDER BY $t_name.today_views DESC $limit;" );
	} elseif( $time = 'month' ) {
		$result = $wpdb->get_results("SELECT * FROM $t_name INNER JOIN $posts_table
									  ON $t_name.post_id = $posts_table.id
									  WHERE ($t_name.month_date > '$month_ago' OR
									  $t_name.month_date = '$month_ago') $lang AND $posts_table.post_type = '$post_type'
									  ORDER BY $t_name.today_views DESC $limit;" );
	} else {
		$result = $wpdb->get_results("SELECT * FROM $t_name INNER JOIN $posts_table
									  ON $t_name.post_id = $posts_table.id AND $posts_table.post_type = '$post_type'
									  $lang_only ORDER BY $t_name.today_views DESC $limit;" );
	}

	return $result;

}